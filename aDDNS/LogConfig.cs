﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace aDDNS
{
    public class LogConfig
    {
        readonly string configPath = System.AppDomain.CurrentDomain.BaseDirectory + "aDDNS_config.xml";
        public string publicIP { get; set; }
        public string acckeyID { get; set; }
        public string acckeySecret { get; set; }
        public string domainName { get; set; }
        public string subDomainName { get; set; }
        public string intervalSec { get; set; }

        public void ReadConfig()
        {
            string[] config = new string[6];
            int i = 0;
            XmlDocument xmlDOC = new XmlDocument();
            if (!File.Exists(configPath))
            {
                SaveConfig();
            }
            xmlDOC.Load(configPath);
            XmlNodeReader readXML = new XmlNodeReader(xmlDOC);

            while (readXML.Read())
            {
                readXML.MoveToElement();
                if (readXML.NodeType == XmlNodeType.Text)
                {
                    config[i] = readXML.Value;
                    //WriteLog(readXML.Value);
                    i++;
                }
            }
            publicIP = config[0];
            acckeyID = config[1];
            acckeySecret = config[2];
            domainName = config[3];
            subDomainName = config[4];
            intervalSec = config[5];
        }

        public void SaveConfig()
        {


            XmlTextWriter textWriter = new XmlTextWriter(configPath, null);
            textWriter.WriteStartDocument();

            textWriter.WriteStartElement("Config");
            //ip
            textWriter.WriteStartElement("publicIP", System.DateTime.Now.ToString());
            textWriter.WriteString(publicIP);
            textWriter.WriteEndElement();
            //acckeyid
            textWriter.WriteStartElement("acckeyid", "");
            textWriter.WriteString(acckeyID);
            textWriter.WriteEndElement();
            //acckeysecret
            textWriter.WriteStartElement("acckeysecret", "");
            textWriter.WriteString(acckeySecret);
            textWriter.WriteEndElement();
            //domain
            textWriter.WriteStartElement("domain", "");
            textWriter.WriteString(domainName);
            textWriter.WriteEndElement();
            //subdomain
            textWriter.WriteStartElement("subdomain", "");
            textWriter.WriteString(subDomainName);
            textWriter.WriteEndElement();
            //interval
            textWriter.WriteStartElement("interval", "");
            textWriter.WriteString(intervalSec);
            textWriter.WriteEndElement();

            textWriter.WriteEndElement();

            textWriter.WriteEndDocument();
            textWriter.Close();
        }

        public void WriteLog(string msg)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "Log";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string logPath = AppDomain.CurrentDomain.BaseDirectory + "Log\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            try
            {
                using (StreamWriter sw = File.AppendText(logPath))
                {
                    sw.WriteLine("消息：" + msg);
                    sw.WriteLine("时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    sw.WriteLine("**************************************************");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (IOException e)
            {
                using (StreamWriter sw = File.AppendText(logPath))
                {
                    sw.WriteLine("异常：" + e.Message);
                    sw.WriteLine("时间：" + DateTime.Now.ToString("yyy-MM-dd HH:mm:ss"));
                    sw.WriteLine("**************************************************");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
        }
    }
}
